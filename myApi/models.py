from django.db import models

# Create your models here.
class MyPokemonList(models.Model):
    id = models.AutoField(primary_key=True)
    pokemonName = models.CharField(max_length=128)
    actualPokemonName = models.CharField(max_length=128)
    class Meta:
        db_table = "my_pokemon_list"