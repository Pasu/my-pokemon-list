#rest_framework
from rest_framework.response import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework import *
from datetime import datetime

#django
from django.db.models import Count
from django.contrib.auth.models import *
from django.contrib.contenttypes.models import *
from django.http import *


#local
from .serializers import *
from .models import *


class MyPokemonList(viewsets.ModelViewSet):
    serializer_class = MyPokemonListSerializer
    queryset = MyPokemonList.objects.all()